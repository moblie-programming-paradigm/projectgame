import 'dart:io';
import 'dart:math';

void main(List<String> arguments) {
  TableMap map = TableMap(10, 10);
  LittleRed littleRed = LittleRed(0, 0, map);
  Home home = Home(9, 9);
  int maxWolf = 20;
  addWolf(map, maxWolf);
  map.setHome(home);
  map.setLittleRed(littleRed);
  while (!map.isFinished) {
    map.showMap();
    // W,a| N,w| E,d| S, s| q: quit
    String direction = stdin.readLineSync()!;
    if (direction == 'q') {
      print("Exit game.");
      break;
    }
    littleRed.walk(direction);
  }
}

void addWolf(TableMap map, int maxWolf) {
  for (int i = 1; i <= maxWolf; i++) {
    Wolf wolf =
        Wolf(Random().nextInt(map.width - 1), Random().nextInt(map.height - 1));
    if (map.objects.contains(wolf)) {
      i--;
    } else {
      map.addObj(wolf);
    }
  }
}

class Obj {
  int x;
  int y;
  String symbol;

  Obj(this.symbol, this.x, this.y);

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }
}

class Wolf extends Obj {
  int x;
  int y;
  String symbol = "W";

  Wolf(this.x, this.y) : super("W", x, y);
}

class Home extends Obj {
  int x;
  int y;
  String symbol = "H";

  Home(this.x, this.y) : super("H", x, y);
}

class LittleRed extends Obj {
  TableMap map;
  late int x;
  late int y;
  int hp = 3;
  String symbol = "R";

  LittleRed(this.x, this.y, this.map) : super("R", x, y);

  bool walk(String direction) {
    switch (direction) {
      case 'N':
      case 'w':
        if (walkN()) return false;
        break;
      case 'S':
      case 's':
        if (walkS()) return false;
        break;
      case 'E':
      case 'd':
        if (walkE()) return false;

        break;
      case 'W':
      case 'a':
        if (walkW()) return false;
        break;
      default:
        return false;
    }
    checkHome();
    checkWolf();
    return true;
  }

  void checkHome() {
    if (map.isHome(x, y)) {
      map.showMap();
      print("Founded Home!!!");
      print("You win.");
      map.isFinished = true;
    }
  }

  void checkWolf() {
    if (map.isWolf(x, y)) {
      print("Founded Wolf!!!");
      hp--;
      print("Current hp = $hp");
      if (hp == 0) {
        map.showMap();
        print("Game over.");
        map.isFinished = true;
      }
    }
  }

  bool canWalk(int x, int y) {
    return map.inMap(x, y);
  }

  bool walkW() {
    if (canWalk(x - 1, y)) {
      x = x - 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    if (canWalk(x + 1, y)) {
      x = x + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    if (canWalk(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkN() {
    if (canWalk(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    return false;
  }
}

class TableMap {
  int width;
  int height;
  late LittleRed littleRed;
  late Home home;
  bool isFinished = false;
  List<Obj> objects = [];
  int objCount = 0;

  TableMap(this.width, this.height);

  void setLittleRed(LittleRed littleRed) {
    this.littleRed = littleRed;
    addObj(littleRed);
  }

  void addObj(Obj obj) {
    objects.add(obj);
    objCount++;
  }

  void printSymbolOn(int x, int y) {
    String symbol = '-';
    for (int o = 0; o < objCount; o++) {
      if (objects[o].isOn(x, y)) {
        symbol = objects[o].symbol;
      }
    }
    stdout.write(symbol);
  }

  void showMap() {
    showTitle();
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        printSymbolOn(x, y);
      }
      showNewLine();
    }
  }

  void showTitle() {
    print("Map");
  }

  void showNewLine() {
    print("");
  }

  void showCell() {
    print("-");
  }

  void showLittleRed() {
    print(littleRed.symbol);
  }

  bool inMap(int x, int y) {
    // x -> 0-(width-1), y -> 0-(height-1)
    return (x >= 0 && x < width) && (y >= 0 && y < height);
  }

  bool isWolf(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is Wolf && objects[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }

  bool isHome(int x, int y) {
    return home.isOn(x, y);
  }

  void setHome(Home home) {
    this.home = home;
    addObj(home);
  }
}
